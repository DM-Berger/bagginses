from utils import stop_tf_warning_spam

stop_tf_warning_spam()

import numpy as np

from sklearn.model_selection import StratifiedShuffleSplit as StratSplit

from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical


def format_data(data: dict, cnn=False) -> (np.array, np.array):
    X, y = data.get("X"), data.get("y")
    X = np.array(np.transpose(X, [2, 0, 1]), dtype=float)
    y = np.array(y.ravel(), dtype=float)
    if cnn:
        # add silly indicator dimension
        X = np.expand_dims(X, X.shape[-1])
        y_hot = to_categorical(y, num_classes=10)
        return X, y, y_hot
    else:
        X = X.reshape([X.shape[0], X.shape[1] * X.shape[2]])
        return X, y


def get_subset(train_size=5000):
    (X_tr, y_tr), (X_test, y_test) = mnist.load_data()
    idx, _ = list(StratSplit(1, len(y_tr) - train_size, train_size).split(X_tr, y_tr))[
        0
    ]

    # idx = np.random.permutation(train_size)
    X_tr = X_tr[idx, :, :]
    # X_test = X_test[idx, :, :]
    y_tr = y_tr[idx]
    # y_test = y_test[idx]
    return (X_tr, y_tr), (X_test, y_test)


def get_mnist_y_test():
    (_, _), (_, y_test) = mnist.load_data()
    return y_test

import numpy as np
import sys
import time

from math import floor
from pathlib import Path
from scipy.stats import mode
from sklearn.model_selection import (
    train_test_split,
    StratifiedKFold,
    StratifiedShuffleSplit as StratSplit,
)
from typing import Dict, List, Tuple, Union
from typing import NewType

# from utils import stop_tf_warning_spam

# stop_tf_warning_spam()
import tensorflow as tf

from tensorflow.keras import losses, optimizers, activations
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN
from tensorflow.keras.datasets import mnist
from tensorflow.keras.layers import (
    Conv2D,
    Flatten,
    BatchNormalization,
)  # we have 2D images
from tensorflow.keras.layers import Dense, MaxPool2D, Dropout
from tensorflow.keras.models import Sequential
from tensorflow.keras.preprocessing.image import ImageDataGenerator

# from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.utils import to_categorical

# local imports

from display import Logger, log_model_options, log_train_options, print_upper_tr
from mnist import get_mnist_y_test, get_subset


Arr = NewType("Arr", np.ndarray)
Model = NewType("Model", type(Sequential))
DropoutRates = NewType("DropoutRates", Tuple[float, float, float, float])
BuilderOptions = NewType(
    "BuilderOptions", Dict[str, Union[DropoutRates, bool, List[float]]]
)
TrainOptions = NewType("TrainOptions", Dict[str, int])

TRAIN_OPTIONS = {"n_models": 9, "max_epochs": 30, "train_size": -1}
MODEL_OPTIONS = {
    "dropouts": (0.2, 0.2, 0.2, 0.1),
    "batchNormalize": False,
    "finalDense": [128],
}

# sys.stdout = Logger(Path("last_output.log"))


def build_cnn(options: BuilderOptions):
    units = 10
    dropouts: DropoutRates = options["dropouts"]
    batchNormalize = options["batchNormalize"]
    finalDense = options["finalDense"]

    model = Sequential()
    model.add(BatchNormalization())

    # 12 nodes/neurons/filters each 5x5, to match MATLAB script
    # https://www.datacamp.com/community/tutorials/convolutional-neural-networks-python
    model.add(
        Conv2D(
            128,
            kernel_size=7,
            input_shape=(28, 28, 1),
            activation=activations.relu,
            data_format="channels_last",
            padding="same",
        )
    )
    # model.add(LeakyReLU(0.1))
    if batchNormalize:
        model.add(BatchNormalization())
    else:
        model.add(Dropout(dropouts[0]))

    model.add(Conv2D(64, kernel_size=3, activation=activations.relu, padding="same"))
    model.add(MaxPool2D())
    if batchNormalize:
        model.add(BatchNormalization())
    else:
        model.add(Dropout(dropouts[1]))

    # model.add(MaxPool2D())
    model.add(Conv2D(64, kernel_size=3, activation=activations.relu))
    model.add(MaxPool2D())
    if batchNormalize:
        model.add(BatchNormalization())
    else:
        model.add(Dropout(dropouts[2]))

    model.add(Conv2D(64, kernel_size=3, activation=activations.relu))
    model.add(Conv2D(64, kernel_size=3, activation=activations.relu))
    # model.add(LeakyReLU(alpha=0.1))
    if batchNormalize:
        model.add(BatchNormalization())
    else:
        model.add(Dropout(dropouts[3]))

    # need flattening layer between a convolution layer and a Dense layer
    model.add(Flatten())
    for size in finalDense:
        model.add(Dense(size))
    # model.add(Flatten())
    model.add(Dense(units=units, activation="softmax"))  # multiclass
    model.compile(
        optimizer=optimizers.RMSprop(),
        # optimizer=optimizers.Adagrad(),
        loss=losses.categorical_crossentropy,
        metrics=["categorical_accuracy"],
    )
    return model


def calculate_error_consistency(
    y_preds: [np.array], y_test: np.array
) -> (np.array, float, np.array, np.array):
    """Given a set of predicted values, `y_preds`, and correct predictions,
    `y_test`, calculate the proportion of samples where the indices of the
    errors (incorrectly predicted values) match the error indices for the other
    predictions. All possible pairings of y_preds are considered.

    ----------------------

    returns: upper triangular matrix of all error consistences, mean of error
    consistencies (computed across the upper triangle), and indices where
    classification is correct across all runs
    """
    idx_error = [(y_pred.ravel() != y_test.ravel()).ravel() for y_pred in y_preds]
    n = len(y_preds)
    consistencies = np.zeros([n, n], dtype=np.float)
    flats = []
    for i, idx1 in enumerate(idx_error):
        for j, idx2 in enumerate(idx_error):
            if j > i:  # only fill upper triangle
                intersect = np.sum(np.logical_and(idx1, idx2))
                union = np.sum(np.logical_or(idx1, idx2))
                consistency = intersect / union

                consistencies[i, j] = consistency
                flats.append(consistency)

    return (
        consistencies,
        np.mean(flats),
        np.percentile(flats, [5, 95]),
        np.array(idx_error).all(0),
    )


def vote_predict_mcr(y_preds: [np.array], y_test: np.array) -> (float, np.array):
    """
    Return the misclassification rate (raw, not as percentage), and
    the vote-predicted labels
    """
    y_preds_all = np.vstack(y_preds).T
    y_vote_pred = mode(np.array(y_preds_all), 1).mode.ravel()
    vote_mcr = 1 - np.mean(y_vote_pred == y_test)

    return vote_mcr, y_vote_pred


class ErrorConsistency:
    def __init__(
        self,
        model_builder,
        model_builder_options: BuilderOptions = {
            "dropouts": (0.2, 0.2, 0.2, 0.1),
            "batchNormalize": False,
            "finalDense": [128],
        },
        train_options: TrainOptions = {
            "n_models": 11,
            "max_epochs": 30,
            "train_size": 1000,
        },
    ):
        self.epoch_starts = []
        self.epochs_elapsed = 0
        self.max_epochs = train_options["max_epochs"]
        self.n_models = train_options["n_models"]
        self.model_builder = model_builder
        self.models = [
            model_builder(model_builder_options) for _ in range(self.n_models)
        ]
        self.model_options = model_options
        self.train_options = train_options

        self.train_size = train_options["train_size"]
        if self.train_size > 0:
            self._data = get_subset(self.train_size)
        else:
            self._data = mnist.load_data()

    @property
    def data(self):

        """ensure we don't run into aliasing issues"""
        (X_tr, y_tr), (X_test, y_test) = self._data
        X_tr = np.array(X_tr, dtype=float)
        X_test = np.array(X_test, dtype=float)
        c = np.copy
        return (c(X_tr), c(y_tr)), (c(X_test), c(y_test))

    @property
    def y_test(self):
        (X_tr, y_tr), (X_test, y_test) = self._data
        return np.copy(y_test)

    def _test_model(self, model: Model, idx_subtrain=None, data=None, augment=None):
        if data is None:
            (X_tr, y_tr), (X_test, y_test) = self.data
        else:
            (X_tr, y_tr), (X_test, y_test) = data

        if idx_subtrain is not None:
            X_tr, y_tr = X_tr[idx_subtrain, :, :], y_tr[idx_subtrain]

        if augment is not None:
            generate = ImageDataGenerator(
                # featurewise_center=True,
                # featurewise_std_normalization=True,
                rotation_range=30,  # max degrees
                width_shift_range=0.3,  # max shift as proportion of image width
                height_shift_range=0.3,
                shear_range=0.5,  # Shear angle in counter-clockwise direction in degrees
                zoom_range=[0.5, 1.1],
                data_format="channels_last",
            )

        X_tr = X_tr.reshape(X_tr.shape[0], 28, 28, 1)
        X_test = X_test.reshape(X_test.shape[0], 28, 28, 1)
        y_tr = to_categorical(y_tr, num_classes=10)
        y_test = to_categorical(y_test, num_classes=10)

        splits = list(StratSplit(2, test_size=0.1, train_size=0.9).split(X_tr, y_tr))[0]
        idx_train, idx_validate = splits

        X_val, y_val = X_tr[idx_validate, :, :, :], y_tr[idx_validate]
        X_tr, y_tr = X_tr[idx_train, :, :, :], y_tr[idx_train]

        callbacks = [
            EarlyStopping(
                baseline=0.13,
                patience=15,
                monitor="val_categorical_accuracy",
                min_delta=0.0004,
                restore_best_weights=True,
            ),
            ReduceLROnPlateau(),
            TimeRemaining(self),
            TerminateOnNaN(),
        ]

        # partitioner = StratifiedKFold(n_splits=5)
        start = time.time()
        if augment is not None:
            generate.fit(X_tr)
            model.fit_generator(
                generate.flow(X_tr, y_tr),
                steps_per_epoch=np.ceil(len(X_tr) / 2),
                epochs=self.max_epochs,
                verbose=1,
                callbacks=callbacks,
                validation_data=(X_val, y_val),
                workers=8,
                use_multiprocessing=True,
            )
        else:
            model.fit(
                X_tr,
                y_tr,
                validation_data=(X_val, y_val),
                epochs=self.max_epochs,
                verbose=1,
                callbacks=callbacks,
            )
        training_time = np.round((time.time() - start) / 60, 2)

        y_pred_scores = model.predict(X_test)
        y_pred = np.argmax(y_pred_scores, -1)  # predicted digit
        y_test = np.argmax(y_test, -1)
        acc = np.mean(y_pred == y_test)
        mcr = np.round(100 * (1 - acc), 2)
        print(f"Total training time: {training_time} minutes")
        print("Holdout error rate:", mcr, "%")

        return y_pred, mcr

    def _test_all_models(
        self,
        models: List[Model],
        idx_subtrains: List[Arr] = None,
        data=None,
        augment=None,
    ) -> Tuple[Arr, Arr, float]:
        n = len(models)

        y_preds, mcrs = list(np.empty(n)), list(np.empty(n))
        iter_times, loop_start = [], time.time()
        if idx_subtrains is None:
            for i, model in enumerate(models):
                iter_start = time.time()
                y_preds[i], mcrs[i] = self._test_model(
                    model, data=data, augment=augment
                )
                iter_times.append(time.time() - iter_start)

                avg_loop_duration = np.mean(np.array(iter_times))
                remain = np.round((n - i) * avg_loop_duration / 60, 2)
                if remain < 1:
                    print(
                        f"\nEstimated Time Remaining: {np.round(remain*60, 0)} seconds\n"
                    )
                else:
                    print(f"\nEstimated Time Remaining: {remain} minutes\n")
        else:
            for i, (model, idx) in enumerate(zip(models, idx_subtrains)):
                iter_start = time.time()
                y_preds[i], mcrs[i] = self._test_model(
                    model, idx, data=data, augment=augment
                )
                iter_times.append(time.time() - iter_start)

                avg_loop_duration = np.mean(np.array(iter_times))
                remain = np.round((n - i) * avg_loop_duration / 60, 2)
                if remain < 1:
                    print(
                        f"\nEstimated Time Remaining: {np.round(remain*60, 0)} seconds\n"
                    )
                else:
                    print(f"\nEstimated Time Remaining: {remain} minutes\n")

        loop_duration = time.time() - loop_start

        return y_preds, mcrs, loop_duration

    def _log_consistency_results(self, summary):
        loop_duration = summary.get("loop_duration")
        error_consistencies = summary.get("error_consistencies")
        mean_consistency = summary.get("mean_consistency")
        percentile95 = summary.get("percentile95")
        always = summary.get("always")
        mcrs = summary.get("mcrs")
        bagged_mcr = summary.get("bagged_mcr")

        if loop_duration is not None:
            print(
                f"Total bagging process time .. {np.round(loop_duration / 60, 1)} minutes."
            )
        if error_consistencies is not None:
            print(f"Error consistencies:")
            print_upper_tr(error_consistencies, decimals=3)
        if mean_consistency is not None:
            print(f"Mean error consistency ...... {np.round(mean_consistency, 3)}")
        if percentile95 is not None:
            print(f"Error consistency 95% CI .... {np.round(percentile95, 3)}")
        if always is not None:
            print(f"Proportion always inconsistent  {np.round(np.mean(always), 3)}")
        if mcrs is not None:
            print(f"Individual error rates ...... {mcrs}")
            print(f"Mean error rate ............. {np.round(np.mean(mcrs), 3)}")
        if bagged_mcr is not None:
            print(f"Bagged error rate ........... {np.round(100*bagged_mcr, 2)}")

    def evaluate(self, augment=None, logging=True):
        (X_tr, y_tr), (X_test, y_test) = self.data
        self.models = [
            self.model_builder(self.model_options) for _ in range(self.n_models)
        ]

        y_preds, mcrs, loop_duration = self._test_all_models(
            self.models, augment=augment
        )
        bagged_mcr, vote_preds = vote_predict_mcr(y_preds, y_test)
        error_consistencies, mean_consistency, percentile95, always = calculate_error_consistency(
            y_preds, y_test
        )

        ensemble_summary = {
            "mcrs": mcrs,
            "loop_duration": loop_duration,
            "bagged_mcr": bagged_mcr,
            "vote_preds": vote_preds,
            "error_consistencies": error_consistencies,
            "mean_consistency": mean_consistency,
            "percentile95": percentile95,
            "always": always,
        }
        if logging:
            timestamp = time.ctime().replace(":", "-").replace(" ", "_")
            sys.stdout = Logger(Path(f"single_model__{timestamp}.log"))
            log_train_options(self.train_options)
            log_model_options(self.model_options)
            print("\nFull config:\n")
            print(self.models[0].get_config())
            print("\n\n")
            print(self.models[0].summary(line_length=120))
            self._log_consistency_results(ensemble_summary)
        return ensemble_summary

    def evaluate_on_subgroups(
        self,
        disjoint_subgroups: int = None,
        train_size=None,
        augment=None,
        logging=True,
    ):
        (X_tr, y_tr), (X_test, y_test) = self.data
        if disjoint_subgroups is not None:
            ss = StratifiedKFold(disjoint_subgroups, shuffle=True)
        elif train_size is not None:
            ss = StratSplit(n_splits=self.n_models, train_size=train_size)
        else:
            raise ValueError(
                "Bad combination of arguments to `evaluate_on_subgroups()`."
                "Must choose one of either `disjoint_subgroups` or `train_size`"
            )

        n_splits = ss.get_n_splits()
        ss = ss.split(X_tr, y_tr)
        self.models = [self.model_builder(self.model_options) for _ in range(n_splits)]
        idxs = [l[0] for l in ss]  # just get train indices

        y_preds, mcrs, loop_duration = self._test_all_models(
            self.models, idxs, augment=augment
        )
        bagged_mcr, vote_preds = vote_predict_mcr(y_preds, y_test)
        error_consistencies, mean_consistency, percentile95, always = calculate_error_consistency(
            y_preds, y_test
        )
        ensemble_summary = {
            "models": self.models,
            "mcrs": mcrs,
            "loop_duration": loop_duration,
            "bagged_mcr": bagged_mcr,
            "vote_preds": vote_preds,
            "error_consistencies": error_consistencies,
            "mean_consistency": mean_consistency,
            "percentile95": percentile95,
            "always": np.mean(always),
        }

        if logging:
            timestamp = time.ctime().replace(":", "-").replace(" ", "_")
            sys.stdout = Logger(Path(f"subgroup_summary__{timestamp}.log"))
            print("\nFull config:\n")
            print(self.models[0].get_config())
            print("\n\n")
            print(self.models[0].summary(line_length=120))
            log_train_options(self.train_options)
            log_model_options(self.model_options)
            self._log_consistency_results(ensemble_summary)
        return ensemble_summary

    def get_prelim_error_set(self, n_models=1, test_size=0.1, logging=True):
        """Use n_models to identify an error set. If n_models == 1, the error
        set is the set of samples for which the classifier makes an error. If
        n_models is greater than 1, the error set is the set of samples
        (indices) for which a bagged model makes errors.
        """
        (X_tr, y_tr), (X_val, y_val) = self.data
        X_tr, X_test, y_tr, y_test = train_test_split(
            X_tr, y_tr, test_size=test_size, stratify=y_tr
        )

        n = n_models
        models = [self.model_builder(self.model_options for _ in range(n))]
        y_preds, mcrs, loop_duration = self._test_all_models(
            models, data=((X_tr, y_tr), (X_test, y_test))
        )
        bagged_mcr, vote_preds = vote_predict_mcr(y_preds, y_test)
        vote_error_idx = (vote_preds != y_test).ravel()
        error_consistencies, mean_consistency, percentile95, always = calculate_error_consistency(
            y_preds, y_test
        )
        summary = {
            "mcrs": mcrs,
            "loop_duration": loop_duration,
            "bagged_mcr": bagged_mcr,
        }

        if logging:
            print("Initial error-set identification:")
            self._log_consistency_results(summary)

        return vote_error_idx, y_preds, y_test


class TimeRemaining(tf.keras.callbacks.Callback):
    def __init__(self, errorConsistency: ErrorConsistency):
        self.max_epochs = errorConsistency.max_epochs
        self.n_models = errorConsistency.n_models
        self.parent = errorConsistency
        super().__init__()

    def on_train_begin(self, logs=None):
        self.model_epoch_starts = []  # epoch start times for this model only
        return super().on_train_begin(logs=logs)

    def on_epoch_begin(self, epoch, logs=None):
        now = time.time()
        self.model_epoch_starts.append(now)
        self.parent.epoch_starts.append(now)
        return super().on_epoch_begin(epoch, logs=logs)

    def on_epoch_end(self, epoch, logs=None):
        self.parent.epochs_elapsed += 1

        if epoch == 0:
            return super().on_epoch_end(epoch, logs=logs)

        durations = (
            np.array(self.model_epoch_starts)[1:]
            - np.array(self.model_epoch_starts)[:-1]
        )
        durations_all = (
            np.array(self.parent.epoch_starts)[1:]
            - np.array(self.parent.epoch_starts)[:-1]
        )
        epochs_remain = self.parent.max_epochs - epoch
        epochs_remain_all = (
            self.parent.n_models * self.parent.max_epochs - self.parent.epochs_elapsed
        )
        mean_dur = np.mean(durations)
        mean_dur_all = np.mean(durations_all)

        remain = epochs_remain * mean_dur
        remain_all = epochs_remain_all * mean_dur_all

        h, h_all = remain / 3600, remain_all / 3600
        m, m_all = 60 * (h - floor(h)), 60 * (h_all - floor(h_all))
        s, s_all = 60 * (m - floor(m)), 60 * (m_all - floor(m_all))

        print(
            f"Estimated time remaining for this model: {floor(h)}h : {floor(m)}m : {floor(s)}s"
        )
        print(
            f"Estimated total time remaining: {floor(h_all)}h : {floor(m_all)}m : {floor(s_all)}s"
        )
        return super().on_epoch_end(epoch, logs=logs)


# instead of recursive implementation, we know counts, if you want 1 bag of 3 3-bags,
# i.e. bag_of(bag_of(3, 9), 9), since all the models are independent, you can just-
# pre run the exact amount you need, and then collect later.
# e.g. ensemble of 3 bags, each bag size n, then you run 3*n models, and just
# partition over models [0:n-1, n:2*n-1, 2*n:3*n-1]
def bag_of_subgroup_bags(
    train_options=TRAIN_OPTIONS, model_options=MODEL_OPTIONS, ensemble_size=3
):
    loop_start = time.time()
    err, summaries = None, []  # save first model to extract test data

    for i in range(ensemble_size):
        ec = ErrorConsistency(build_cnn, model_options, train_options)
        summary = ec.evaluate_on_subgroups(train_size=0.333, logging=False)
        summaries.append(summary)
        if i == 0:
            err = ec

    loop_duration = time.time() - loop_start

    # actually calculate the ensemble performance
    y_test = get_mnist_y_test()
    vote_preds = [summary["vote_preds"] for summary in summaries]
    ensemble_mcr, ensemble_vote_preds = vote_predict_mcr(vote_preds, y_test)
    ensemble_consistencies, ensemble_mean_consistency, percentile95, ensemble_always = calculate_error_consistency(
        vote_preds, y_test
    )

    mean_mcrs = [np.round(np.mean(summary["mcrs"]), 3) for summary in summaries]
    bagged_mcrs = np.array([summary["bagged_mcr"] for summary in summaries])
    mean_consistencies = [summary["mean_consistency"] for summary in summaries]
    always = np.array([100 * summary["always"] for summary in summaries])

    ensemble_summary = {
        "loop_duration": loop_duration,
        "bagged_mcr": ensemble_mcr,
        "vote_preds": vote_preds,
        "error_consistencies": ensemble_consistencies,
        "mean_consistency": ensemble_mean_consistency,
        "percentile95": percentile95,
        "always": ensemble_always,
    }

    timestamp = time.ctime().replace(":", "-").replace(" ", "_")
    sys.stdout = Logger(Path(f"bag_o_bags_summary__{timestamp}.log"))
    print("\nFull config:\n")
    print(summaries[0][0].get_config())
    print("\n\n")
    print(summaries[0][0].summary(line_length=120))
    log_train_options(train_options)
    log_model_options(model_options)
    err._log_consistency_results(ensemble_summary)

    print("\n\n")
    print("Bag o' Bags Error Consistency Report")
    print("------------------------------------")

    print("\nIndividual Model Summaries:\n")
    for i, summary in enumerate(summaries):
        print(f"\n\nBagged model {i}:")
        err._log_consistency_results(summary)

    print("\nOverview of Individual Bagged Models:\n")
    print(f"Mean consistencies for each model ...... {np.round(mean_consistencies, 3)}")
    print(
        f"Average of mean consistencies .......... {np.round(np.mean(mean_consistencies), 3)}"
    )
    print(f"Mean mcrs (no bags) .................... {mean_mcrs}")
    print(
        f"Average bagged misclassification rate .. {np.round(np.mean(100*bagged_mcrs), 3)}"
    )
    print(f"Percent always inconsistent .............. {np.round(always, 3)}")
    print(f"Bagged misclassification rates ......... {np.round(100*bagged_mcrs, 3)}")

    print(f"\n\nEnsemble Statistics")
    print("------------------------------------")

    err._log_consistency_results(ensemble_summary)
    pass


def ensemble_performance(summaries: [dict], loop_duration: float) -> dict:
    y_test = get_mnist_y_test()  # TODO: move this to be passed in?
    vote_preds = [summary["vote_preds"] for summary in summaries]
    ensemble_mcr, ensemble_vote_preds = vote_predict_mcr(vote_preds, y_test)
    ensemble_consistencies, ensemble_mean_consistency, percentile95, ensemble_always = calculate_error_consistency(
        vote_preds, y_test
    )

    mean_mcrs = [np.round(np.mean(summary["mcrs"]), 3) for summary in summaries]
    bagged_mcrs = np.array([summary["bagged_mcr"] for summary in summaries])
    mean_consistencies = [summary["mean_consistency"] for summary in summaries]
    always = np.array([100 * summary["always"] for summary in summaries])

    return {
        "loop_duration": loop_duration,
        "bagged_mcr": ensemble_mcr,
        "vote_preds": vote_preds,
        "error_consistencies": ensemble_consistencies,
        "mean_consistency": ensemble_mean_consistency,
        "percentile95": percentile95,
        "always": ensemble_always,
    }


# A = np.array([[1, 2, 3, 4], [0, 6, 7, 8], [0, 0, 6, 8], [0, 0, 0, 9]])
# B = A * 1.8712319873712937
# C = np.array([[0, 2, 3, 4], [0, 0, 7, 8], [0, 0, 0, 8], [0, 0, 0, 0]])
# print_upper_tr(A)
# print_upper_tr(B)
# print_upper_tr(C)
train_options = {"n_models": 5, "max_epochs": 30, "train_size": -1}
model_options = {
    "dropouts": (0.2, 0.2, 0.2, 0.1),
    "batchNormalize": True,
    "finalDense": [128],
}
# bag_of_subgroup_bags(train_options, model_options, ensemble_size=7)

# err = ErrorConsistency(build_cnn, model_options, train_options).evaluate_on_subgroups(
#     train_size=0.9, logging=True
# )


err = ErrorConsistency(build_cnn, model_options, train_options).evaluate(logging=True)

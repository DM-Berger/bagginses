import matplotlib.pyplot as plt
import numpy as np
import sys

from pathlib import Path


# redirect stdout using the trick at:
# https://stackoverflow.com/a/14906787
class Logger(object):
    def __init__(self, filepath: Path):
        self.terminal = sys.stdout
        self.log = open(str(filepath.absolute().resolve()), "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass


def print_upper_tr(A: np.array, decimals=3):
    A = np.asarray(np.round(A, decimals), dtype=str)
    cell_size = np.max([len(s) for s in A.flat])
    cell = "{:3}"
    if cell_size == 1:
        cell = "{:3}"
    elif cell_size == 2:
        cell = "{:4}"
    elif cell_size == 3:
        cell = "{:5}"
    elif cell_size == 4:
        cell = "{:6}"
    elif cell_size == 5:
        cell = "{:7}"
    elif cell_size == 6:
        cell = "{:8}"
    else:
        raise ValueError("Too many decimals to print cleanly")

    for row in A:
        row_str = ""
        for val in row:
            if np.allclose(0, float(val)):
                row_str += cell.format("")
            else:
                row_str += cell.format(val)
        print(row_str)

        # i = 0
        # while np.allclose(0, float(row[i])):  # find first non-zero value
        #     i = i + 1
        # left_pad = " " * (cell_size) * i
        # for
        # vals = str.join(" ", list(row[i:]))
        # print(f"{left_pad}{vals}")


def log_train_options(options):
    n = options["n_models"]
    epochs_per_model = options["max_epochs"]
    train_size = options["train_size"]
    size = "all"
    if train_size > 0:
        size = f"{np.round(100 * train_size / 60000, 1)}%"

    print(f"Size of training subgroup ............. {size}")
    print(f"Number of models per ensemble to bag .. {n}")
    print(f"Max epochs per individual model ....... {epochs_per_model}")


def log_model_options(options):
    dropout_rates = options["dropouts"]
    batchNormalize = options["batchNormalize"]
    dense = options["finalDense"]
    if len(dense) > 1:
        denseString = f"{len(dense)} layers with sizes {dense}"
    else:
        denseString = f"{len(dense)} layer with size {dense}"

    if batchNormalize:
        print(f"Using batch normalization after convolution layers.")
    else:
        print(f"Using dropout rates {dropout_rates} after convolution layers.")
    print(f"Final dense / fully-connected layers prior to softmax: {denseString}")


def ensemble_report(train_options, model_options, ensemble_size):
    pass


def plot_model_history(history, metrics=["categorical_accuracy"]):
    num_plots = len(metrics) + 1
    for i in range(1, num_plots):
        metric = metrics[i - 1]
        plt.subplot(1, num_plots, i)
        if metric == "accuracy" or metric == "acc":
            plt.title("Error Rate")
            plt.plot(1 - np.array(history.history["acc"]))
            plt.plot(1 - np.array(history.history["val_acc"]))
        else:
            plt.title(f"{metric}")
            plt.plot(1 - np.array(history.history[f"{metric}"]))
            plt.plot(1 - np.array(history.history[f"val_{metric}"]))

        if i == 0:
            plt.xlabel("Epoch")
            plt.legend(["Train", "Test"], loc="best")

    # plt.subplot(1, 4, 2)
    # plt.title("Categorical Accuracy")
    # plt.plot(history.history["categorical_accuracy"])
    # plt.plot(history.history["val_categorical_accuracy"])
    # plt.xlabel("Epoch")

    # plt.subplot(1, 4, 3)
    # plt.title("Sparse Categorical Accuracy")
    # plt.plot(history.history["sparse_categorical_accuracy"])
    # plt.plot(history.history["val_sparse_categorical_accuracy"])
    # plt.xlabel("Epoch")

    plt.subplot(1, num_plots, num_plots)
    plt.title("Loss")
    plt.plot(history.history["loss"])
    plt.plot(history.history["val_loss"])
    plt.xlabel("Epoch")

    plt.show()
